package view;

import connection.IConnection;
import controller.BrokerController;
import model.Broker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.*;

public class BrokerView {

    public BrokerView(IConnection conn, Broker b)
    {
        JFrame window = new JFrame("Broker");
        JPanel northPanel = new JPanel();
        northPanel.setLayout(new GridLayout(1,1));
        northPanel.setBackground(Color.ORANGE);
        JLabel label = new JLabel("Bine ai venit, " + b.getUsername() + "!");
        northPanel.add(label);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new GridLayout(1,5));
        southPanel.setBackground(Color.ORANGE);

        JButton createButton = new JButton("Create Account");
        createButton.addActionListener(new BrokerController(conn,b));
        JButton buyButton = new JButton("Buy Instruments");
        buyButton.addActionListener(new BrokerController(conn,b));
        JButton sellButton = new JButton("Sell Instruments");
        sellButton.addActionListener(new BrokerController(conn,b));
        JButton viewTradesButton = new JButton("View Trades");
        viewTradesButton.addActionListener(new BrokerController(conn,b));
        JButton eodButton = new JButton("EoD");
        eodButton.addActionListener(new BrokerController(conn,b));

        JPanel centerPanel = new JPanel();
        centerPanel.setBackground(Color.ORANGE);

        southPanel.add(createButton);
        southPanel.add(buyButton);
        southPanel.add(sellButton);
        southPanel.add(viewTradesButton);
        southPanel.add(eodButton);

        window.setLayout(new BorderLayout());
        window.setBackground(Color.ORANGE);
        window.add(northPanel, BorderLayout.NORTH);
        window.add(centerPanel, BorderLayout.CENTER);
        window.add(southPanel, BorderLayout.SOUTH);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(300,150);

        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setVisible(true);
        window.setResizable(true);
        
    }
}
