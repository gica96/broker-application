package view;

import connection.IConnection;
import controller.BuyController;
import model.Broker;
import model.FinancialInstrument;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class BuyView {
    
    public BuyView(IConnection connection, Broker broker)
    {
        JFrame window = new JFrame("Buy Instruments");
        
        List<FinancialInstrument> instruments = new ArrayList<FinancialInstrument>();
        instruments.add(new FinancialInstrument("GoPRO 123", 100.0));
        instruments.add(new FinancialInstrument("Sony 500", 50.50));
        instruments.add(new FinancialInstrument("SteelSeries Sensei", 400.0));
        instruments.add(new FinancialInstrument("Razer Naga", 500.0));

        JPanel northPanel = new JPanel();
        northPanel.setLayout(new GridLayout(1,1));

        JComboBox<String> nameBox = new JComboBox<String>();
        nameBox.addItem("GoPRO 123");
        nameBox.addItem("Sony 500");
        nameBox.addItem("SteelSeries Sensei");
        nameBox.addItem("Razer Naga");
   

        northPanel.add(nameBox);
        
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(2,2));
        JLabel label = new JLabel("Cantitatea: ");
        centerPanel.add(label);
        JTextField field = new JTextField();
        centerPanel.add(field);
        JLabel money = new JLabel("Banii dvs: ");
        centerPanel.add(money);
        JLabel moneyValue = new JLabel(String.valueOf(broker.getMoney()));
        centerPanel.add(moneyValue);

        String[] data = new String[] {"Instrument", "Price"};

        DefaultTableModel model = new DefaultTableModel(data,0) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        for(FinancialInstrument f : instruments)
        {
            model.addRow(new Object[] {f.getName(), f.getPrice()});
        }

        JTable table = new JTable();
        table.setModel(model);
        JScrollPane scroll = new JScrollPane(table);
        
        JPanel westPanel = new JPanel();
        westPanel.add(scroll);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new GridLayout(1,1));

        JButton buy = new JButton("Buy Instrument");
       
        buy.addActionListener(new BuyController(window, connection, broker, field, nameBox, instruments));

        southPanel.add(buy);

        window.setBackground(Color.ORANGE);
        window.add(northPanel, BorderLayout.NORTH);
        window.add(centerPanel, BorderLayout.CENTER);
        window.add(southPanel, BorderLayout.SOUTH);
        window.add(westPanel, BorderLayout.WEST);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(300,150);

        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setVisible(true);
        window.setResizable(true);
    }
}
