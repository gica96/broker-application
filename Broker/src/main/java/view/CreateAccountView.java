package view;

import connection.IConnection;
import controller.CreateAccountController;

import javax.swing.*;
import java.awt.*;

public class CreateAccountView {
    
    public CreateAccountView(IConnection connection)
    {
        JFrame window = new JFrame("Create Account");

        JLabel userLabel = new JLabel("Username");
        JTextField usernameField = new JTextField();
        JLabel passLabel = new JLabel("Password");
        JTextField passField = new JTextField();
        JLabel emptyLabel = new JLabel("");

        JButton loginButton = new JButton("Create Account!");

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(3,2));
        centerPanel.setBackground(Color.GREEN);

        centerPanel.add(userLabel);
        centerPanel.add(usernameField);
        centerPanel.add(passLabel);
        centerPanel.add(passField);
        centerPanel.add(emptyLabel);
        centerPanel.add(loginButton);
        loginButton.addActionListener(new CreateAccountController(connection, usernameField, passField, window));


        window.add(centerPanel);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(300,150);

        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setVisible(true);
        window.setResizable(true);
    }
}
