package view;

import connection.IConnection;
import controller.SellController;
import model.Broker;
import model.FinancialInstrument;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class SellView {

    public SellView(IConnection connection, Broker broker)
    {
        JFrame window = new JFrame("Sell Instruments");

        List<FinancialInstrument> instruments = broker.getInstruments();
        
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(2,2));
        JLabel label = new JLabel("Cantitatea: ");
        centerPanel.add(label);
        JTextField field = new JTextField();
        centerPanel.add(field);
        JLabel money = new JLabel("Banii dvs: ");
        centerPanel.add(money);
        JLabel moneyValue = new JLabel(String.valueOf(broker.getMoney()));
        centerPanel.add(moneyValue);

        String[] data = new String[] {"Instrument", "Price", "Quantity", "Trade"};

        DefaultTableModel model = new DefaultTableModel(data,0) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        for(FinancialInstrument f : instruments)
        {
            model.addRow(new Object[] {f.getName(), f.getPrice(), f.getStock(), f.getIdTrade()});
        }

        JTable table = new JTable();
        table.setModel(model);
        JScrollPane scroll = new JScrollPane(table);

        JPanel westPanel = new JPanel();
        westPanel.add(scroll);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new GridLayout(1,1));

        JButton sell = new JButton("Sell Instrument");

        sell.addActionListener(new SellController(window, connection, broker, field, table, instruments));

        southPanel.add(sell);

        window.setBackground(Color.ORANGE);
        window.add(centerPanel, BorderLayout.CENTER);
        window.add(southPanel, BorderLayout.SOUTH);
        window.add(westPanel, BorderLayout.WEST);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(300,150);

        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setVisible(true);
        window.setResizable(true);
        
    }
    
}
