package view;


import controller.MainPageController;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class MainPage {

    public MainPage()
    {
        JFrame window = new JFrame("Main Page");

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(1,1));
        centerPanel.setBackground(Color.CYAN);
        
        JButton loginBroker = new JButton("Login Broker");
        loginBroker.addActionListener(new MainPageController(window));
        
        centerPanel.add(loginBroker);
        
        window.add(centerPanel);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(510,75);

        window.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        window.setVisible(true);
        window.setResizable(true);
    }
}
