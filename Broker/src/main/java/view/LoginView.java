package view;

import connection.IConnection;
import controller.LoginController;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoginView {
    
    private IConnection connection;

    public LoginView(IConnection connection)
    {
        this.connection = connection;
        JFrame window = new JFrame("Login");

        JLabel userLabel = new JLabel("Username");
        JTextField usernameField = new JTextField();
        JLabel passLabel = new JLabel("Password");
        JTextField passField = new JTextField();
        JLabel emptyLabel = new JLabel("");

        JButton loginButton = new JButton("Login!");

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(3,2));
        centerPanel.setBackground(Color.CYAN);

        centerPanel.add(userLabel);
        centerPanel.add(usernameField);
        centerPanel.add(passLabel);
        centerPanel.add(passField);
        centerPanel.add(emptyLabel);
        centerPanel.add(loginButton);
        loginButton.addActionListener(new LoginController(window, connection, usernameField, passField));


        window.add(centerPanel);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(300,150);

        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setVisible(true);
        window.setResizable(true);

    }
    
    
}
