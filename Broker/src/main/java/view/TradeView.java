package view;

import connection.IConnection;
import controller.CancelController;
import controller.SellController;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TradeView {
    
    public TradeView(IConnection iConnection, Broker broker)
    {
        List<Trade> trades = broker.getTrades();

        String[] data = new String[] {"Trade ID", "Date", "Type", "Broker ID"};

        DefaultTableModel model = new DefaultTableModel(data,0) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        for(Trade t : trades)
        {
            model.addRow(new Object[] {t.getIdTrade(), t.getDateOfTrade(), t.getTypeOfTrade(), t.getIdBroker_trade()});
        }

        JTable table = new JTable();
        table.setModel(model);
        JScrollPane scroll = new JScrollPane(table);
        
        JPanel centerPanel = new JPanel();
        centerPanel.add(scroll);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new GridLayout(1,1));

        JButton cancel = new JButton("Cancel Trade");

        cancel.addActionListener(new CancelController(iConnection, broker, table));

        southPanel.add(cancel);
        
        JFrame window = new JFrame("Trades");

        window.setBackground(Color.ORANGE);
        window.add(centerPanel, BorderLayout.CENTER);
        window.add(southPanel, BorderLayout.SOUTH);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(300,150);

        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setVisible(true);
        window.setResizable(true);

    }
}
