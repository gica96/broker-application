package view;

import connection.IConnection;
import controller.EodController;
import model.Broker;

import javax.swing.*;
import java.awt.*;

public class EodView {
    
    public EodView(IConnection connection, Broker broker)
    {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1,2));
        
        JButton sellReport = new JButton("Sell Report");
        sellReport.addActionListener(new EodController(connection, broker));
        JButton buyReport = new JButton("Buy Report");
        buyReport.addActionListener(new EodController(connection, broker));
        
        panel.add(sellReport);
        panel.add(buyReport);

        JFrame window = new JFrame("Trades");

        window.setBackground(Color.ORANGE);
        window.add(panel, BorderLayout.CENTER);
        window.pack();
        window.setLocation(400, 250);
        window.setSize(300,150);

        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setVisible(true);
        window.setResizable(true);
    }
}
