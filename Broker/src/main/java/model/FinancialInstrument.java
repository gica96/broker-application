package model;

import javax.persistence.*;

@Entity
@Table(name = "FinancialInstrument")
public class FinancialInstrument {
    
    @Id
    @Column(name = "idFinancialInstrument")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idFinancialInstrument;
    
    @Column(name = "Name")
    private String name;
    
    @Column(name = "Price")
    private double price;
    
    @Column(name = "Stock")
    private int stock;
    
    @Column(name = "idBroker_financial")
    private int idBroker_financial;
    
    @Column(name = "idTrade_financial")
    private int idTrade;
    
    public FinancialInstrument()
    {
        //
    }

    public FinancialInstrument(int id, String name, double price, int stock, int idBroker_financial) 
    {
        this.idFinancialInstrument = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.idBroker_financial = idBroker_financial;
    }

    public FinancialInstrument(int id, String name, double price, int stock, int idBroker_financial, int idTrade)
    {
        this.idFinancialInstrument = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.idBroker_financial = idBroker_financial;
        this.idTrade = idTrade;
    }

    public FinancialInstrument(String name, double price, int stock, int idBroker_financial)
    {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.idBroker_financial = idBroker_financial;
    }
    
    public FinancialInstrument(String name, double price)
    {
        this.name = name;
        this.price = price;
    }

    public int getIdFinancialInstrument() {
        return idFinancialInstrument;
    }

    public void setIdFinancialInstrument(int idFinancialInstrument) 
    {
        this.idFinancialInstrument = idFinancialInstrument;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getIdBroker_financial() {
        return idBroker_financial;
    }

    public void setIdBroker_financial(int idBroker_financial) 
    {
        this.idBroker_financial = idBroker_financial;
    }
    
    public void setIdTrade(int value)
    {
        this.idTrade = value;
    }
    
    public int getIdTrade()
    {
        return this.idTrade;
    }
}
