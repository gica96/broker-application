package model;


import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "Trade")
public class Trade {
    
    @Id
    @Column(name = "idTrade")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idTrade;
    
    @Column(name = "date_of_trade")
    private Date dateOfTrade;
    
    @Column(name = "type_of_trade")
    private String typeOfTrade;
    
    @Column(name = "idBroker_trade")
    private int idBroker_trade;
    
    @OneToMany
    @JoinColumn(name = "idTrade_financial")
    private List<FinancialInstrument> tradingInstruments;
    
    public Trade() 
    {
        
    }

    public Trade(int id, Date dateOfTrade, String typeOfTrade, int idBroker_trade) 
    {
        this.idTrade = id;
        this.dateOfTrade = dateOfTrade;
        this.typeOfTrade = typeOfTrade;
        this.idBroker_trade = idBroker_trade;
    }

    public Trade(Date dateOfTrade, String typeOfTrade, int idBroker_trade)
    {
        this.dateOfTrade = dateOfTrade;
        this.typeOfTrade = typeOfTrade;
        this.idBroker_trade = idBroker_trade;
    }

    public int getIdTrade() {
        return idTrade;
    }

    public void setIdTrade(int idTrade) {
        this.idTrade = idTrade;
    }

    public Date getDateOfTrade() {
        return dateOfTrade;
    }

    public void setDateOfTrade(Date dateOfTrade) {
        this.dateOfTrade = dateOfTrade;
    }

    public String getTypeOfTrade() {
        return typeOfTrade;
    }

    public void setTypeOfTrade(String typeOfTrade) {
        this.typeOfTrade = typeOfTrade;
    }

    public int getIdBroker_trade() {
        return idBroker_trade;
    }

    public void setIdBroker_trade(int idBroker_trade) {
        this.idBroker_trade = idBroker_trade;
    }
    
}
