package model;

import javax.persistence.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@javax.persistence.Table(name = "Broker")
public class Broker {
    
    @Id
    @Column(name = "idBroker")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idBroker;
    
    @Column(name = "username")
    private String username;
    
    @Column(name = "password")
    private String password;
    
    @Column(name = "money")
    private double money;
    
    @OneToMany
    @JoinColumn(name = "idBroker_financial")
    private List<FinancialInstrument> instruments;
    
    @OneToMany
    @JoinColumn(name = "idBroker_trade")
    private List<Trade> trades;
    

    public Broker(int idBroker, String username, String password, double money) 
    {
        this.idBroker = idBroker;
        this.username = username;
        this.password = password;
        this.money = money;
        this.instruments = new ArrayList<FinancialInstrument>();
        this.trades = new ArrayList<Trade>();
    }
    
    public void renewInstruments()
    {
        this.instruments = new ArrayList<FinancialInstrument>();
    }
    
    public void renewTrades()
    {
        this.trades = new ArrayList<Trade>();
    }
    
    public Broker(String username, String password)
    {
        this.username = username;
        this.password = password;
    }
    
    public Broker()
    {
        //
    }

    public int getIdBroker() {
        return idBroker;
    }

    public void setIdBroker(int idBroker) {
        this.idBroker = idBroker;
    }

    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
    
    public List<FinancialInstrument> getInstruments()
    {
        return this.instruments;
    }
    
    public List<Trade> getTrades()
    {
        return this.trades;
    }
    
    public void addInstrument(FinancialInstrument i)
    {
        this.instruments.add(i);
    }
    
    public void addTrade(Trade trade)
    {
        this.trades.add(trade);
    }
}
