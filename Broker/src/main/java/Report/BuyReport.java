package Report;

import model.Trade;

import java.util.ArrayList;
import java.util.List;

public class BuyReport implements IReport {

    private List<Trade> buyTrades;

    public BuyReport()
    {
        this.buyTrades = new ArrayList<Trade>();
    }

    public List<Trade> getSellTrades()
    {
        return this.buyTrades;
    }

    public void addTrade(Trade trade)
    {
        this.buyTrades.add(trade);
    }
    
    public String getReportType() 
    {
        return "Buy Reports for today: ";
    }
}
