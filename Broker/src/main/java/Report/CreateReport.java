package Report;

public class CreateReport {
    
    public IReport factoryMethod(String s)
    {
        if(s.equals("Sell Report"))
        {
            return new SellReport();
        }
        else if(s.equals("Buy Report"))
        {
            return new BuyReport();
        }
        else 
        {
            return null;
        }
    }
}
