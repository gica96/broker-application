package Report;

import model.Trade;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;

import java.util.ArrayList;
import java.util.List;

public class SellReport implements IReport {
    
    private List<Trade> sellTrades;
    
    public SellReport()
    {
        this.sellTrades = new ArrayList<Trade>();
    }
    
    public List<Trade> getSellTrades()
    {
        return this.sellTrades;
    }
    
    public void setSellTrades(List<Trade> list)
    {
        this.sellTrades = list;
    }

    public String getReportType() 
    {
        return "Sell Reports for today: ";
    }
}
