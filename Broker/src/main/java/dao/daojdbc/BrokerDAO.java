package dao.daojdbc;

import connection.JDBCConnection;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BrokerDAO implements BrokerIDAO 
{
    
    public boolean findBrokerByUsername(String username, String pass) {
        String findNameAndPass = "SELECT Broker.username, Broker.password FROM Broker";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = connection.prepareStatement(findNameAndPass);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                String theName = rs.getString("username");
                String thePass = rs.getString("password");

                if (theName.equals(username)) {
                    if (thePass.equals(pass)) {
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
        System.out.println("Brokerul cu username-ul: " + username + " si cu parola: " + pass + " nu a fost gasit!");
        return false;
    }

    public int getBrokerId(String username) {
        int id = 0;
        String findId = "SELECT Broker.idBroker from Broker WHERE Broker.username = '" + username + "'";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = connection.prepareStatement(findId);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                id = rs.getInt("idBroker");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
        return id;
    }

    public void loadInstruments(Broker broker) 
    {
        broker.renewInstruments();
        int id = getBrokerId(broker.getUsername());

        String findBill = "select FinancialInstrument.idFinancialInstrument, FinancialInstrument.Name, FinancialInstrument.Price, FinancialInstrument.Stock, FinancialInstrument.idBroker_financial, FinancialInstrument.idTrade_financial FROM FinancialInstrument where FinancialInstrument.idBroker_financial ='" + id + "'";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = connection.prepareStatement(findBill);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                int instrumentId = rs.getInt("idFinancialInstrument");
                String name = rs.getString("Name");
                double price = rs.getDouble("Price");
                int stock = rs.getInt("Stock");
                int idBroker = rs.getInt("idBroker_financial");
                int idTrade = rs.getInt("idTrade_financial");
                FinancialInstrument instrument = new FinancialInstrument(instrumentId, name, price, stock, idBroker, idTrade);
                broker.addInstrument(instrument);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
    }

    public void loadMoney(Broker broker) {
        double money = 0.0;
        String findMoney = "SELECT Broker.Money from Broker WHERE Broker.username = '" + broker.getUsername() + "'";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = connection.prepareStatement(findMoney);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                money = rs.getDouble("Money");
                broker.setMoney(money);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
    }

    public void insertBroker(String username, String password) {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement("INSERT INTO Broker(idBroker, username, password, money) VALUES (?,?,?,?)");
            stmt.setNull(1, Types.INTEGER);
            stmt.setString(2, username);
            stmt.setString(3, password);
            stmt.setDouble(4, 1000000.0);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeStatement((Statement) stmt);
            JDBCConnection.closeConnection(connection);
        }
    }

    public void insertTrade(Trade trade, Broker broker) {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement("INSERT INTO Trade(idTrade, date_of_trade, type_of_trade, idBroker_trade) VALUES (?,?,?,?)");
            stmt.setNull(1, Types.INTEGER);
            stmt.setDate(2, trade.getDateOfTrade());
            stmt.setString(3, trade.getTypeOfTrade());
            stmt.setInt(4, broker.getIdBroker());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeStatement(stmt);
            JDBCConnection.closeConnection(connection);
        }
    }

    public int getIdTrade(Broker broker) {
        int id = 0;
        String findId = "SELECT Trade.idTrade from Trade WHERE Trade.idBroker_trade = '" + broker.getIdBroker() + "'";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = connection.prepareStatement(findId);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                id = rs.getInt("idTrade");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
        return id;
    }

    public void insertInstrument(FinancialInstrument financialInstrument) {
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement("INSERT INTO FinancialInstrument(idFinancialInstrument, Name, Price, Stock, idBroker_financial, idTrade_financial ) VALUES (?,?,?,?,?,?)");
            stmt.setNull(1, Types.INTEGER);
            stmt.setString(2, financialInstrument.getName());
            stmt.setDouble(3, financialInstrument.getPrice());
            stmt.setInt(4, financialInstrument.getStock());
            stmt.setInt(5, financialInstrument.getIdBroker_financial());
            stmt.setInt(6, financialInstrument.getIdTrade());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeStatement(stmt);
            JDBCConnection.closeConnection(connection);
        }
    }

    public void updateBrokerPrice(Broker broker, double price) {
        int brokerId = broker.getIdBroker();
        String update = "update Broker set Broker.money ='" + price + "'" + "where Broker.idBroker ='" + brokerId + "'";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;

        try 
        {
            findStatement = connection.prepareStatement(update);
            findStatement.executeUpdate();
        } catch (SQLException ex) 
        {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
    }

    public void updateInstrument(String name, int newStock, int idTrade) 
    {
        String update = "update FinancialInstrument set FinancialInstrument.Stock ='" + newStock + "'" + "where FinancialInstrument.Name ='" + name + "'" + "and FinancialInstrument.idTrade_financial = '" + idTrade +"'";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;

        try 
        {
            findStatement = connection.prepareStatement(update);
            findStatement.executeUpdate();
        } catch (SQLException ex) 
        {
            ex.printStackTrace();
        } finally 
        {
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
    }
    
    public void loadTrades(Broker broker)
    {
        broker.renewTrades();
        int id = getBrokerId(broker.getUsername());

        String findTrades = "select Trade.idTrade, Trade.date_of_trade, Trade.type_of_trade, Trade.idBroker_trade FROM Trade where Trade.idBroker_trade ='" + id + "'";

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try 
        {
            findStatement = connection.prepareStatement(findTrades);
            rs = findStatement.executeQuery();
            while (rs.next()) 
            {
                int tradeId = rs.getInt("idTrade");
                Date date = rs.getDate("date_of_trade");
                String type = rs.getString("type_of_trade");
                int idBroker = rs.getInt("idBroker_trade");
                Trade trade = new Trade(tradeId, (java.sql.Date)date, type, idBroker);
                broker.addTrade(trade);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
    }
    
    public void deleteInstrument(int idTrade)
    {
        String find = "Delete from FinancialInstrument WHERE FinancialInstrument.idTrade_financial = '" + idTrade + "'";
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement deleteStatement = null;
        try
        {
            deleteStatement = connection.prepareStatement(find);
            deleteStatement.executeUpdate();
        }
        catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            JDBCConnection.closeStatement(deleteStatement);
            JDBCConnection.closeConnection(connection);
        }
    }
    
    public void deleteTrade(int idTrade)
    {
        String find = "Delete from Trade WHERE Trade.idTrade = '" + idTrade + "'";
        Connection connection = JDBCConnection.getConnection();
        PreparedStatement deleteStatement = null;
        try
        {
            deleteStatement = connection.prepareStatement(find);
            deleteStatement.executeUpdate();
        }
        catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            JDBCConnection.closeStatement(deleteStatement);
            JDBCConnection.closeConnection(connection);
        }
    }
    
    public List<Trade> getSellTrades(Broker broker)
    {
        List<Trade> trades = new ArrayList<Trade>();
        int id = getBrokerId(broker.getUsername());
        String s = "Sell Trade";
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        String findTrades = "select Trade.idTrade, Trade.date_of_trade, Trade.type_of_trade, Trade.idBroker_trade FROM Trade where Trade.idBroker_trade ='" + id + "'" + "and Trade.type_of_Trade = '" + s + "'" + "and Trade.date_of_trade = '" + date +"'" ;

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try
        {
            findStatement = connection.prepareStatement(findTrades);
            rs = findStatement.executeQuery();
            while (rs.next())
            {
                int tradeId = rs.getInt("idTrade");
                Date dateNew = rs.getDate("date_of_trade");
                String type = rs.getString("type_of_trade");
                int idBroker = rs.getInt("idBroker_trade");
                Trade trade = new Trade(tradeId, (java.sql.Date)dateNew, type, idBroker);
                trades.add(trade);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
        return trades;
    }

    public List<Trade> getBuyTrades(Broker broker)
    {
        List<Trade> trades = new ArrayList<Trade>();
        int id = getBrokerId(broker.getUsername());
        String s = "Buy Trade";
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        String findTrades = "select Trade.idTrade, Trade.date_of_trade, Trade.type_of_trade, Trade.idBroker_trade FROM Trade where Trade.idBroker_trade ='" + id + "'" + "and Trade.type_of_Trade = '" + s + "'" + "and Trade.date_of_trade = '" + date +"'" ;

        Connection connection = JDBCConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try
        {
            findStatement = connection.prepareStatement(findTrades);
            rs = findStatement.executeQuery();
            while (rs.next())
            {
                int tradeId = rs.getInt("idTrade");
                Date dateNew = rs.getDate("date_of_trade");
                String type = rs.getString("type_of_trade");
                int idBroker = rs.getInt("idBroker_trade");
                Trade trade = new Trade(tradeId, (java.sql.Date)dateNew, type, idBroker);
                trades.add(trade);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JDBCConnection.closeResultSet(rs);
            JDBCConnection.closeStatement(findStatement);
            JDBCConnection.closeConnection(connection);
        }
        return trades;
    }
}