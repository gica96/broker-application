package dao.daojdbc;

import model.Broker;
import model.FinancialInstrument;
import model.Trade;

import java.util.List;

public interface BrokerIDAO {
    
    boolean findBrokerByUsername(String username, String pass);
    int getBrokerId(String username);
    void loadInstruments(Broker broker);
    void loadMoney(Broker broker);
    void insertBroker(String username, String password);
    void insertTrade(Trade trade, Broker broker);
    int getIdTrade(Broker broker);
    void insertInstrument(FinancialInstrument instrument);
    void updateBrokerPrice(Broker broker, double price);
    void updateInstrument(String name, int newStock, int idTrade);
    void loadTrades(Broker broker);
    void deleteInstrument(int idTrade);
    void deleteTrade(int idTrade);
    List<Trade> getSellTrades(Broker broker);
    List<Trade> getBuyTrades(Broker broker);
}
