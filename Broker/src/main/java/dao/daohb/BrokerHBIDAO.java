package dao.daohb;

import model.Broker;
import model.FinancialInstrument;
import model.Trade;
import java.util.List;

public interface BrokerHBIDAO {
    
    Broker findBrokerByUsername(String username, String password);
    void loadInstruments(Broker broker);
    int getBrokerId(String username);
    void loadTrades(Broker broker);
    void insertBroker(String username, String password);
    void loadMoney(Broker broker);
    void updateBrokerPrice(Broker broker, double newMoney);
    void insertTrade(Trade trade);
    int getIdTrade(Broker broker);
    void insertInstrument(FinancialInstrument financialInstrument);
    void updateInstrument(String name, int newStock, int idTrade);
    void deleteInstrument(int idTrade);
    void deleteTrade(int idTrade);
    List<Trade> getSellTrades(Broker broker);
    List<Trade> getBuyTrades(Broker broker);
}
