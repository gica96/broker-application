package dao.daohb;

import connection.HBConnection;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.Calendar;
import java.util.List;


public class BrokerHBDAO implements BrokerHBIDAO {
    
    public Broker findBrokerByUsername(String username, String password)
    {
        Broker broker = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "from Broker where username = :username and password = :password";
            Query query = sessionObj.createQuery(queryString);
            query.setString("username", username);
            query.setString("password", password);
            broker = (Broker)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("1st Mistake");
            ex.printStackTrace();
        }
        return broker;
    }
    
    public int getBrokerId(String username)
    {
        Broker broker = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Broker where username = :username";
            Query query = sessionObj.createQuery(queryString);
            query.setString("username", username);
            broker = (Broker)query.uniqueResult();
        }
        catch(Exception ex) 
        {
            System.out.println("1st Mistake");
            ex.printStackTrace();
        }
        return broker.getIdBroker();
    }
    
    public void loadInstruments(Broker broker)
    {
        broker.renewInstruments();
        int idBroker = getBrokerId(broker.getUsername());
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM FinancialInstrument where idBroker_financial = :idBroker_financial";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBroker_financial", idBroker);
            List<FinancialInstrument> list = query.list();
            for(FinancialInstrument financialInstrument : list)
            {
                broker.addInstrument(financialInstrument);
            }
        }
        catch(Exception ex)
        {
            System.out.println("2nd Mistake");
            ex.printStackTrace();
        }
    }
    
    public void loadTrades(Broker broker)
    {
        broker.renewTrades();
        int idBroker = getBrokerId(broker.getUsername());
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Trade where idBroker_trade = :idBroker_trade";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBroker_trade", idBroker);
            List<Trade> list = query.list();
            for(Trade trade : list)
            {
                broker.addTrade(trade);
            }
        }
        catch(Exception ex)
        {
            System.out.println("3rd Mistake");
            ex.printStackTrace();
        }
    }
    
    public void insertBroker(String username, String password)
    {
        Broker broker = new Broker(username, password);
        broker.setMoney(1000000);

        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            sessionObj.save(broker);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("4th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void loadMoney(Broker broker)
    {
        Broker newBroker = null;

        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Broker where username = :username";
            Query query = sessionObj.createQuery(queryString);
            query.setString("username", broker.getUsername());
            newBroker = (Broker)query.uniqueResult();
            broker.setMoney(newBroker.getMoney());
        }
        catch(Exception ex)
        {
            System.out.println("4th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void updateBrokerPrice(Broker broker, double newMoney)
    {
        int id = broker.getIdBroker();
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Broker where idBroker = :idBroker";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBroker", id);
            Broker newBroker = (Broker)query.uniqueResult();
            newBroker.setMoney(newMoney);
            sessionObj.update(newBroker);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("5th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void insertTrade(Trade trade)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            sessionObj.save(trade);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("6th Mistake");
            ex.printStackTrace();
        }
    }
    
    public int getIdTrade(Broker broker)
    {
        int id = broker.getIdBroker();
        int toReturn = 0;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Trade where idBroker_trade = :idBroker_trade";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBroker_trade", id);
            Trade trade = (Trade)query.list().get(query.list().size()-1);
            toReturn = trade.getIdTrade();
        }
        catch(Exception ex)
        {
            System.out.println("7th Mistake");
            ex.printStackTrace();
        }
        return toReturn;
    }
    
    public void insertInstrument(FinancialInstrument financialInstrument)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            sessionObj.save(financialInstrument);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("8th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void updateInstrument(String name, int newStock, int idTrade)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM FinancialInstrument where Name = :Name and idTrade_financial = :idTrade_financial";
            Query query = sessionObj.createQuery(queryString);
            query.setString("Name", name);
            query.setInteger("idTrade_financial", idTrade);
            FinancialInstrument financialInstrument = (FinancialInstrument)query.uniqueResult();
            financialInstrument.setStock(newStock);
            sessionObj.update(financialInstrument);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("9th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void deleteInstrument(int idTrade)
    {
        try 
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            Query query = sessionObj.createQuery("DELETE FinancialInstrument where idTrade_financial = " + idTrade);
            query.executeUpdate();
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("10th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void deleteTrade(int idTrade)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            Query query = sessionObj.createQuery("DELETE Trade where idTrade = " + idTrade);
            query.executeUpdate();
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("11th Mistake");
            ex.printStackTrace();
        }
    }

    public List<Trade> getSellTrades(Broker broker) 
    {
        int id = getBrokerId(broker.getUsername());
        List<Trade> trades = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            String queryString = "from Trade where idBroker_trade = :idBroker_trade and type_of_trade = :type_of_trade and date_of_trade = :date_of_trade";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBroker_trade", id);
            query.setString("type_of_trade", "Sell Trade");
            query.setDate("date_of_trade", date);
            trades = query.list();
        }
        catch(Exception ex)
        {
            System.out.println("12th Mistake");
            ex.printStackTrace();
        }
        return trades;
    }

    public List<Trade> getBuyTrades(Broker broker)
    {
        int id = getBrokerId(broker.getUsername());
        List<Trade> trades = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String s = "Buy Trade";
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            String queryString = "from Trade where idBroker_trade = :idBroker_trade and type_of_trade = :type_of_trade and date_of_trade = :date_of_trade";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBroker_trade", id);
            query.setString("type_of_trade", s);
            query.setDate("date_of_trade", date);
            trades = query.list();
        }
        catch(Exception ex)
        {
            System.out.println("13th Mistake");
            ex.printStackTrace();
        }
        return trades;
    }
}
