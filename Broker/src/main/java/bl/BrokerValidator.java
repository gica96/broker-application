package bl;

import java.util.regex.Pattern;

public class BrokerValidator {

    public boolean validateId(int id)
    {
        if(id <= 0)
        {
            return false;
        }
        return true;
    }

    public boolean validateName(String name)
    {
        if(name == null) // nume null
        {
            return false;
        }
        else if(name.length() > 50) // lungimea numelui > 50
        {
            return false;
        }
        else if(name.charAt(0) < 65 || name.charAt(0) > 90) // nu incepe cu litera mare
        {
            return false;
        }
        else // verificam daca numele contine altceva decat literele de la A-Z sau a-z
        {
            for(int i = 1 ; i < name.length(); i++)
            {
                if(name.charAt(i) < 65)
                {
                    if(name.charAt(i) != 32)
                    {
                        return false;
                    }
                }
                else if(name.charAt(i) > 90 && name.charAt(i) < 97)
                {
                    return false;
                }
                else if(name.charAt(i) > 122)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validatePassword(String password)
    {
        if(password.length() > 15) // lungimea parolei
        {
            return false;
        }

        return true;
    }

    public boolean validatePhoneNumber(String number)
    {
        if(number.length() != 10) // daca numarul nu e fix de 10 cifre -> invalidare
        {
            return false;
        }
        else
        {
            for(int i = 0; i < 10 ; i++)
            {
                if(number.charAt(i) < 48 || number.charAt(i) > 57) // daca contine altceva decat cfire -> invalidare
                {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validateAdress(String adress)
    {
        if(adress.length() > 50)
        {
            return false;
        }

        return true;
    }

    public boolean validateEmail(String email)
    {
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(emailPattern);
        if(!pattern.matcher(email).matches())
        {
            return false;
        }
        return true;
    }

    //length >= 3, caractere valide: a-z, A-Z, 0-9, points, dashes and underscores
    public boolean validateUsername(String username)
    {
        String usernamePattern = "^[a-zA-Z0-9._-]{3,}$";
        Pattern pattern = Pattern.compile(usernamePattern);
        if(!pattern.matcher(username).matches())
        {
            return false;
        }
        return true;
    }

    public boolean validateIdCard(String card)
    {
        if(card.length() != 6)
        {
            return false;
        }
        else
        {
            for(int i = 0; i < 6 ; i++)
            {
                if(card.charAt(i) < 48 || card.charAt(i) > 57) // daca contine altceva decat cfire -> invalidare
                {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validateClientNumber(String number)
    {
        if(number.length() != 7)
        {
            return false;
        }
        else
        {
            for(int i = 0; i < 7 ; i++)
            {
                if(number.charAt(i) < 48 || number.charAt(i) > 57) // daca contine altceva decat cfire -> invalidare
                {
                    return false;
                }
            }
        }
        return true;
    }
}
