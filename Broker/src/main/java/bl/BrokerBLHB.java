package bl;

import dao.daohb.BrokerHBDAO;
import dao.daohb.BrokerHBIDAO;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;

import java.util.List;


public class BrokerBLHB {
    
    private BrokerHBIDAO brokerHBIDAO;
    
    public BrokerBLHB()
    {
        brokerHBIDAO = new BrokerHBDAO();
    }
    
    public Broker findBrokerByUsername(String username, String password)
    {
        return(brokerHBIDAO.findBrokerByUsername(username, password));
    }
    
    public void loadInstruments(Broker broker)
    {
        brokerHBIDAO.loadInstruments(broker);
    }
    
    public void loadTrades(Broker broker)
    {
        brokerHBIDAO.loadTrades(broker);
    }
    
    public void insertBroker(String username, String password)
    {
        brokerHBIDAO.insertBroker(username, password);
    }
    
    public void loadMoney(Broker broker)
    {
        brokerHBIDAO.loadMoney(broker);
    }
    
    public int getBrokerId(String username)
    {
        return brokerHBIDAO.getBrokerId(username);
    }
    
    public void updateBrokerPrice(Broker broker, double newMoney)
    {
        brokerHBIDAO.updateBrokerPrice(broker, newMoney);
    }
    
    public void insertTrade(Trade trade)
    {
        brokerHBIDAO.insertTrade(trade);
    }
    
    public int getIdTrade(Broker broker)
    {
        return brokerHBIDAO.getIdTrade(broker);
    }
    
    public void insertInstrument(FinancialInstrument financialInstrument)
    {
        brokerHBIDAO.insertInstrument(financialInstrument);
    }
    
    public void updateInstrument(String name, int newStock, int idTrade)
    {
        brokerHBIDAO.updateInstrument(name, newStock, idTrade);
    }
    
    public void deleteInstrument(int idTrade)
    {
        brokerHBIDAO.deleteInstrument(idTrade);
    }
    
    public void deleteTrade(int idTrade)
    {
        brokerHBIDAO.deleteTrade(idTrade);
    }
    
    public List<Trade> getSellTrades(Broker broker)
    {
        return brokerHBIDAO.getSellTrades(broker);
    }

    public List<Trade> getBuyTrades(Broker broker)
    {
        return brokerHBIDAO.getBuyTrades(broker);
    }    
}
