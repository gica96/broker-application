package bl;


import dao.daojdbc.BrokerDAO;
import dao.daojdbc.BrokerIDAO;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;

import java.util.List;

public class BrokerBL {
    
    private BrokerIDAO brokerDAO;
    
    public BrokerBL()
    {
        brokerDAO = new BrokerDAO();
    }

    public boolean findBrokerByUsernameJDBC(Broker b)
    {
        return(brokerDAO.findBrokerByUsername(b.getUsername(), b.getPassword()));
    }
    
    public void loadInstruments(Broker broker)
    {
        brokerDAO.loadInstruments(broker);
    }
    
    public void loadMoney(Broker broker)
    {
        brokerDAO.loadMoney(broker);
    }
    
    public void insertBroker(String username, String password)
    {
        brokerDAO.insertBroker(username, password);
    }
    
    public int getBrokerId(String username)
    {
        return(brokerDAO.getBrokerId(username));
    }
    
    public void insertTrade(Trade trade, Broker broker)
    {
        brokerDAO.insertTrade(trade, broker);
    }
    
    public int getIdTrade(Broker broker)
    {
        return(brokerDAO.getIdTrade(broker));
    }
    
    public void insertInstrument(FinancialInstrument instrument)
    {
        brokerDAO.insertInstrument(instrument);
    }
    
    public void updateBrokerPrice(Broker broker, double price)
    {
        brokerDAO.updateBrokerPrice(broker, price);
    }
    
    public void updateInstrument(String name, int newStock, int idTrade)
    {
        brokerDAO.updateInstrument(name, newStock, idTrade);
    }
    
    public void loadTrades(Broker broker)
    {
        brokerDAO.loadTrades(broker);
    }
    
    public void deleteInstrument(int idTrade)
    {
        brokerDAO.deleteInstrument(idTrade);
    }
    
    public void deleteTrade(int idTrade)
    {
        brokerDAO.deleteTrade(idTrade);
    }
    
    public List<Trade> getSellTrades(Broker broker)
    {
        return brokerDAO.getSellTrades(broker);
    }
    
    public List<Trade> getBuyTrades(Broker broker)
    {
        return brokerDAO.getBuyTrades(broker);
    }
}
