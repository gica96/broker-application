package connection;

public class Creator {
    
    public IConnection factoryMethod(int number)
    {
        if(number == 0) 
        {
            return new HBConnection();
        }
        else if(number == 1) 
        {
            return new JDBCConnection();
        }
        return null;
    }
}
