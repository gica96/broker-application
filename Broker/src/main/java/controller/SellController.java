package controller;

import bl.BrokerBL;
import bl.BrokerBLHB;
import connection.HBConnection;
import connection.IConnection;
import connection.JDBCConnection;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

public class SellController implements ActionListener{

    private JFrame window;
    private IConnection connection;
    private Broker broker;
    private JTextField field;
    private JTable table;
    private List<FinancialInstrument> instruments;

    public SellController(JFrame window, IConnection connection, Broker broker, JTextField field, JTable table, List<FinancialInstrument> list)
    {
        this.window = window;
        this.connection = connection;
        this.broker = broker;
        this.field = field;
        this.table = table;
        this.instruments = list;
    }


    public void actionPerformed(ActionEvent e) 
    {
        String s = e.getActionCommand();
        if(s.equals("Sell Instrument"))
        {
            if(connection instanceof JDBCConnection)
            {
                int row = this.table.getSelectedRow();
                int stock = (Integer)this.table.getValueAt(row, 2);
                if(this.field.getText().equals(""))
                {
                    System.out.println("Field-ul nu poate fi gol!");
                }
                else if(Integer.parseInt(this.field.getText()) <= 0)
                {
                    System.out.println("Numarul introdus nu poate fi 0 sau negativ");
                }
                else if(Integer.parseInt(field.getText()) > stock)
                {
                    
                    System.out.println("Nu puteti vinde mai mult decat aveti!");
                    
                }
                else {
                    BrokerBL brokerBL = new BrokerBL();
                    int newStock = 0;
                    for (FinancialInstrument financialInstrument : this.instruments) {
                        if (financialInstrument.getName().equals((this.table.getValueAt(row, 0)))) {
                            if (financialInstrument.getIdTrade() == (Integer) this.table.getValueAt(row, 3)) {
                                newStock = financialInstrument.getStock() - Integer.parseInt(this.field.getText());
                                financialInstrument.setStock(newStock);
                            }
                        }
                    }

                    double newMoney = this.broker.getMoney() + (Double) this.table.getValueAt(row, 1) * Integer.parseInt(this.field.getText());
                    //update broker's price in DB
                    brokerBL.updateBrokerPrice(broker, newMoney);

                    Date date = new Date(Calendar.getInstance().getTime().getTime());
                    Trade trade = new Trade(date, "Sell Trade", broker.getIdBroker());
                    //insert new trade into Trade table
                    brokerBL.insertTrade(trade, broker);

                    //update financial instrument
                    int idTrade = (Integer) this.table.getValueAt(row, 3);

                    String name = (String) this.table.getValueAt(row, 0);
                    brokerBL.updateInstrument(name, newStock, idTrade);
                    this.window.dispose();
                }
            }
            else if(connection instanceof HBConnection)
            {
                int row = this.table.getSelectedRow();
                int stock = (Integer)this.table.getValueAt(row, 2);
                if(this.field.getText().equals(""))
                {
                    System.out.println("Field-ul nu poate fi gol!");
                }
                else if(Integer.parseInt(this.field.getText()) <= 0)
                {
                    System.out.println("Numarul introdus nu poate fi 0 sau negativ");
                }
                else if(Integer.parseInt(field.getText()) > stock)
                {

                    System.out.println("Nu puteti vinde mai mult decat aveti!");

                }
                else 
                {
                    BrokerBLHB brokerBLHB = new BrokerBLHB();
                    int newStock = 0;
                    for (FinancialInstrument financialInstrument : this.instruments) {
                        if (financialInstrument.getName().equals((this.table.getValueAt(row, 0)))) {
                            if (financialInstrument.getIdTrade() == (Integer) this.table.getValueAt(row, 3)) {
                                newStock = financialInstrument.getStock() - Integer.parseInt(this.field.getText());
                                financialInstrument.setStock(newStock);
                            }
                        }
                    }
                    double newMoney = this.broker.getMoney() + (Double) this.table.getValueAt(row, 1) * Integer.parseInt(this.field.getText());
                    //update broker's price in DB
                    brokerBLHB.updateBrokerPrice(broker, newMoney);
                    Date date = new Date(Calendar.getInstance().getTime().getTime());
                    Trade trade = new Trade(date, "Sell Trade", broker.getIdBroker());
                    //insert new trade into Trade table
                    brokerBLHB.insertTrade(trade);
                    //update financial instrument
                    int idTrade = (Integer) this.table.getValueAt(row, 3);

                    String name = (String) this.table.getValueAt(row, 0);
                    brokerBLHB.updateInstrument(name, newStock, idTrade);
                    this.window.dispose();
                }
            }
        }
    }
}
