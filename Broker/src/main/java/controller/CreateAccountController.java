package controller;

import bl.BrokerBL;
import bl.BrokerBLHB;
import bl.BrokerValidator;
import connection.HBConnection;
import connection.IConnection;
import connection.JDBCConnection;
import org.hibernate.engine.transaction.internal.jta.JtaIsolationDelegate;
import org.hibernate.event.service.internal.EventListenerServiceInitiator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateAccountController implements ActionListener {
    
    private IConnection connection;
    private JTextField username;
    private JTextField pass;
    private JFrame window;
    
    public CreateAccountController(IConnection connection, JTextField username, JTextField pass, JFrame window)
    {
        this.connection = connection;
        this.username = username;
        this.pass = pass;
        this.window = window;
    }

    public void actionPerformed(ActionEvent e) 
    {
        String s = e.getActionCommand();
        if(s.equals("Create Account!")) 
        {
            if(connection instanceof HBConnection)
            {
                BrokerValidator brokerValidator = new BrokerValidator();
                if(brokerValidator.validateUsername(username.getText()))
                {
                    if(brokerValidator.validatePassword(pass.getText()))
                    {
                        BrokerBLHB brokerBLHB = new BrokerBLHB();
                        brokerBLHB.insertBroker(username.getText(), pass.getText());
                        this.window.dispose();
                    }
                    else
                    {
                        System.out.println("Parola invalida ca format!");
                    }
                }
                else
                {
                    System.out.println("Format invalid la username!");
                }
            }
            else if(connection instanceof JDBCConnection) 
            {
                BrokerValidator brokerValidator = new BrokerValidator();
                if(brokerValidator.validateUsername(username.getText())) 
                {
                    if(brokerValidator.validatePassword(pass.getText()))
                    {
                        BrokerBL brokerBL = new BrokerBL();
                        brokerBL.insertBroker(username.getText(), pass.getText());
                        this.window.dispose();
                    }
                    else
                    {
                        System.out.println("Parola invalida ca format!");
                    }
                }
                else
                {
                    System.out.println("Format invalid la username!");
                }
            }
        }
    }
}
