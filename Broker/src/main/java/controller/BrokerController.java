package controller;

import bl.BrokerBL;
import bl.BrokerBLHB;
import connection.HBConnection;
import connection.IConnection;
import connection.JDBCConnection;
import model.Broker;
import view.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BrokerController implements ActionListener {
    
    private IConnection connection;
    private Broker broker;
    
    public BrokerController(IConnection connection, Broker broker)
    {
        this.connection = connection;
        this.broker = broker;
    }
    
    public void actionPerformed(ActionEvent e) 
    {
        String s = e.getActionCommand();
        if(connection instanceof JDBCConnection)
        {
            if(s.equals("Create Account"))
            {
                new CreateAccountView(connection);
            }
            else if(s.equals("Buy Instruments"))
            {
                BrokerBL brokerBL = new BrokerBL();
                brokerBL.loadMoney(broker);
                int id = brokerBL.getBrokerId(broker.getUsername());
                broker.setIdBroker(id);
                new BuyView(connection, broker);
            }
            else if(s.equals("Sell Instruments"))
            {
                BrokerBL brokerBL = new BrokerBL();
                brokerBL.loadMoney(broker);
                brokerBL.loadInstruments(broker);
                int id = brokerBL.getBrokerId(broker.getUsername());
                broker.setIdBroker(id);
                new SellView(connection, broker);
            }
            else if(s.equals("View Trades"))
            {
                BrokerBL brokerBL = new BrokerBL();
                int id = brokerBL.getBrokerId(broker.getUsername());
                broker.setIdBroker(id);
                brokerBL.loadTrades(broker);
                new TradeView(connection, broker);
            }
            else if(s.equals("EoD"))
            {
                new EodView(connection, broker);
            }
        }
        else if(connection instanceof HBConnection)
        {
            if(s.equals("Create Account"))
            {
                new CreateAccountView(connection);
            }
            else if(s.equals("Buy Instruments"))
            {
                BrokerBLHB brokerBLHB = new BrokerBLHB();
                brokerBLHB.loadMoney(broker);
                int id = brokerBLHB.getBrokerId(broker.getUsername());
                broker.setIdBroker(id);
                new BuyView(connection, broker);
            }
            else if(s.equals("Sell Instruments"))
            {
                BrokerBLHB brokerBLHB = new BrokerBLHB();
                brokerBLHB.loadMoney(broker);
                brokerBLHB.loadInstruments(broker);
                int id = brokerBLHB.getBrokerId(broker.getUsername());
                broker.setIdBroker(id);
                new SellView(connection, broker);
            }
            else if(s.equals("View Trades"))
            {
                BrokerBLHB brokerBLHB = new BrokerBLHB();
                int id = brokerBLHB.getBrokerId(broker.getUsername());
                broker.setIdBroker(id);
                brokerBLHB.loadTrades(broker);
                new TradeView(connection, broker);
            }
            else if(s.equals("EoD"))
            {
                new EodView(connection, broker);
            }
        }
        
    }
}
