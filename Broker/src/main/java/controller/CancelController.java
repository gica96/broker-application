package controller;

import bl.BrokerBL;
import bl.BrokerBLHB;
import com.sun.corba.se.pept.transport.InboundConnectionCache;
import connection.HBConnection;
import connection.IConnection;
import connection.JDBCConnection;
import model.Broker;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CancelController implements ActionListener {
    
    private IConnection connection;
    private JTable table;
    private Broker broker;

    
    public CancelController(IConnection connection, Broker broker, JTable table)
    {
        this.connection = connection;
        this.broker = broker;
        this.table = table;
    }

    public void actionPerformed(ActionEvent e) 
    {
        String s = e.getActionCommand();
        if(connection instanceof JDBCConnection)
        {
            if(s.equals("Cancel Trade"))
            {
                int row = table.getSelectedRow();
                int idTrade = (Integer)table.getValueAt(row, 0);
                BrokerBL brokerBL = new BrokerBL();
                brokerBL.deleteInstrument(idTrade);
                brokerBL.deleteTrade(idTrade);
            }
        }
        else if(connection instanceof HBConnection)
        {
            if(s.equals("Cancel Trade"))
            {
                int row = table.getSelectedRow();
                int idTrade = (Integer)table.getValueAt(row, 0);
                BrokerBLHB brokerBLHB = new BrokerBLHB();
                brokerBLHB.deleteInstrument(idTrade);
                brokerBLHB.deleteTrade(idTrade);
            }
        }
    }
}
