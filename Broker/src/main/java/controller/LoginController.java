package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import bl.BrokerBL;
import bl.BrokerBLHB;
import connection.HBConnection;
import connection.IConnection;
import connection.JDBCConnection;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;
import view.BrokerView;


public class LoginController implements ActionListener{

    private IConnection connection;
    private JTextField username;
    private JTextField pass;
    private JFrame loginFrame;

    public LoginController(JFrame f, IConnection type, JTextField username, JTextField pass)
    {
        this.loginFrame = f;
        this.connection = type;
        this.username = username;
        this.pass = pass;
    }
    
    public void actionPerformed(ActionEvent evt)
    {
       String s = evt.getActionCommand();
       if(s.equals("Login!")) 
       {
           Broker broker = new Broker(username.getText(), pass.getText());
           if(connection instanceof HBConnection)
           {
               BrokerBLHB brokerHB = new BrokerBLHB();
               if(brokerHB.findBrokerByUsername(broker.getUsername(), broker.getPassword()) == null)
               {
                   System.out.println("Acest broker nu exista!");
               }
               else
               {
                   brokerHB.loadInstruments(broker);
                   brokerHB.loadTrades(broker);
                   new BrokerView(connection, broker);
                   this.loginFrame.dispose();
               }
           }
           else if(connection instanceof JDBCConnection)
           {
               BrokerBL brokerBL = new BrokerBL();
               if(brokerBL.findBrokerByUsernameJDBC(broker))
               {
                   brokerBL.loadInstruments(broker);
                   brokerBL.loadTrades(broker);
                   new BrokerView(connection, broker);
                   this.loginFrame.dispose();
               }
           }
       }
    }

}
