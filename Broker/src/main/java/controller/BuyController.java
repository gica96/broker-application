package controller;

import bl.BrokerBL;
import bl.BrokerBLHB;
import connection.HBConnection;
import connection.IConnection;
import connection.JDBCConnection;
import model.Broker;
import model.FinancialInstrument;
import model.Trade;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.List;
import java.sql.Date;

public class BuyController implements ActionListener {
    
    private JFrame window;
    private IConnection connection;
    private Broker broker;
    private JTextField field;
    private JComboBox<String> nameBox;
    private List<FinancialInstrument> instruments;
    
    public BuyController(JFrame window, IConnection connection, Broker broker, JTextField field, JComboBox<String> nameBox, List<FinancialInstrument> list)
    {
        this.window = window;
        this.connection = connection;
        this.broker = broker;
        this.field = field;
        this.nameBox = nameBox;
        this.instruments = list;
    }


    public void actionPerformed(ActionEvent e) 
    {
        String s = e.getActionCommand();
        if(s.equals("Buy Instrument")) 
        {
            if(connection instanceof JDBCConnection) 
            {
                if(this.field.getText().equals(""))
                {
                    System.out.println("Field-ul nu poate fi gol!");
                }
                else if(Integer.parseInt(this.field.getText()) <= 0)
                {
                    System.out.println("Numarul introdus nu poate fi 0 sau negativ");
                }
                else
                {
                    double price = 0.0;
                    for(FinancialInstrument financialInstrument : this.instruments) 
                    {
                        if(financialInstrument.getName().equals(nameBox.getSelectedItem().toString())) 
                        {
                            price = financialInstrument.getPrice();
                            break;
                        }
                    }
                    FinancialInstrument newInstrument = new FinancialInstrument(nameBox.getSelectedItem().toString(), price, Integer.parseInt(this.field.getText()), broker.getIdBroker());
                    
                    
                    BrokerBL brokerBL = new BrokerBL();
                    double newMoney = this.broker.getMoney() - newInstrument.getPrice() * newInstrument.getStock();
                    //update broker's price in DB
                    brokerBL.updateBrokerPrice(broker, newMoney);
                    
                    Date date = new Date(Calendar.getInstance().getTime().getTime());
                    Trade trade = new Trade(date, "Buy Trade", broker.getIdBroker());
                    //insert new trade into Trade table
                    brokerBL.insertTrade(trade, broker);

                    //insert the item into FinancialInstrument table
                    int idTrade = brokerBL.getIdTrade(broker);
                    newInstrument.setIdTrade(idTrade);
                    this.broker.addInstrument(newInstrument);
                    brokerBL.insertInstrument(newInstrument);
                    this.window.dispose();
                }
            }
            else if(connection instanceof HBConnection)
            {
                if(this.field.getText().equals(""))
                {
                    System.out.println("Field-ul nu poate fi gol!");
                }
                else if(Integer.parseInt(this.field.getText()) <= 0)
                {
                    System.out.println("Numarul introdus nu poate fi 0 sau negativ");
                }
                else 
                {
                    double price = 0.0;
                    for(FinancialInstrument financialInstrument : this.instruments)
                    {
                        if(financialInstrument.getName().equals(nameBox.getSelectedItem().toString()))
                        {
                            price = financialInstrument.getPrice();
                            break;
                        }
                    }
                    FinancialInstrument newInstrument = new FinancialInstrument(nameBox.getSelectedItem().toString(), price, Integer.parseInt(this.field.getText()), broker.getIdBroker());
                    BrokerBLHB brokerBLHB = new BrokerBLHB();
                    double newMoney = this.broker.getMoney() - newInstrument.getPrice() * newInstrument.getStock();
                    System.out.println(newMoney);
                    //update broker's price in DB
                    brokerBLHB.updateBrokerPrice(broker, newMoney);

                    Date date = new Date(Calendar.getInstance().getTime().getTime());
                    Trade trade = new Trade(date, "Buy Trade", broker.getIdBroker());
                    trade.setIdBroker_trade(broker.getIdBroker());
                    //insert new trade into Trade table
                    brokerBLHB.insertTrade(trade);
                    int idTrade = brokerBLHB.getIdTrade(broker);
                    newInstrument.setIdTrade(idTrade);
                    this.broker.addInstrument(newInstrument);
                    brokerBLHB.insertInstrument(newInstrument);
                    this.window.dispose();
                }
            }
            
        }
    }
}
