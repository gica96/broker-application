package controller;

import Report.CreateReport;
import Report.IReport;
import Report.SellReport;
import bl.BrokerBL;
import bl.BrokerBLHB;
import connection.HBConnection;
import connection.IConnection;
import connection.JDBCConnection;
import model.Broker;
import model.Trade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class EodController implements ActionListener {

    private IConnection connection;
    private Broker broker;

    public EodController(IConnection connection, Broker broker) {
        this.connection = connection;
        this.broker = broker;
    }

    public void actionPerformed(ActionEvent e) 
    {
        String s = e.getActionCommand();
        if (connection instanceof JDBCConnection) 
        {
            if (s.equals("Sell Report")) 
            {
                CreateReport createReport = new CreateReport();
                IReport report = createReport.factoryMethod(s);


                BrokerBL brokerBL = new BrokerBL();
                List<Trade> sellTrades = brokerBL.getSellTrades(broker);
                System.out.println(report.getReportType());
                for (Trade trade : sellTrades) 
                {
                    System.out.println(trade.getIdTrade() + " " + trade.getTypeOfTrade() + " " + trade.getDateOfTrade() + " " + trade.getIdBroker_trade());
                }
            } 
            else if (s.equals("Buy Report")) 
            {
                CreateReport createReport = new CreateReport();
                IReport report = createReport.factoryMethod(s);


                BrokerBL brokerBL = new BrokerBL();
                List<Trade> buyTrades = brokerBL.getBuyTrades(broker);
                System.out.println(report.getReportType());
                for (Trade trade : buyTrades) {
                    System.out.println(trade.getIdTrade() + " " + trade.getTypeOfTrade() + " " + trade.getDateOfTrade() + " " + trade.getIdBroker_trade());
                }
            }
        }
        else if(connection instanceof HBConnection)
        {
            if(s.equals("Sell Report"))
            {
                CreateReport createReport = new CreateReport();
                IReport report = createReport.factoryMethod(s);


                BrokerBLHB brokerBLHB = new BrokerBLHB();
                List<Trade> sellTrades = brokerBLHB.getSellTrades(broker);
                System.out.println(report.getReportType());
                for (Trade trade : sellTrades)
                {
                    System.out.println(trade.getIdTrade() + " " + trade.getTypeOfTrade() + " " + trade.getDateOfTrade() + " " + trade.getIdBroker_trade());
                }
            }
            else if(s.equals("Buy Report"))
            {
                CreateReport createReport = new CreateReport();
                IReport report = createReport.factoryMethod(s);


                BrokerBLHB brokerBLHB = new BrokerBLHB();
                List<Trade> buyTrades = brokerBLHB.getBuyTrades(broker);
                System.out.println(report.getReportType());
                for (Trade trade : buyTrades)
                {
                    System.out.println(trade.getIdTrade() + " " + trade.getTypeOfTrade() + " " + trade.getDateOfTrade() + " " + trade.getIdBroker_trade());
                }
            }
           
        }
    }
}
            

