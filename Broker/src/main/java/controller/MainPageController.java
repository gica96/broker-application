package controller;

import connection.Creator;
import connection.HBConnection;
import connection.IConnection;
import view.LoginView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;

public class MainPageController implements ActionListener{

    private JFrame frame;

    public MainPageController(JFrame f)
    {
        this.frame = f;
    }

    public void actionPerformed(ActionEvent evt)
    {
        String str = evt.getActionCommand();
        Random rn = new Random();
        int random = rn.nextInt(1-0 + 1) + 0;
        if(str.equals("Login Broker"))
        {
            Creator creator = new Creator();
            IConnection connection = creator.factoryMethod(random);
            System.out.println(connection.connectionType());
            new LoginView(connection);
            this.frame.dispose();
        }
    }
}
