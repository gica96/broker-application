import bl.BrokerBL;
import bl.BrokerBLHB;
import model.Broker;
import model.Trade;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TestClass {
    
    @Test
    public void findBrokerByUsername()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        BrokerBL brokerBL = new BrokerBL();
        assertEquals(true, brokerBL.findBrokerByUsernameJDBC(broker));
    }
    
    @Test
    public void getBrokerById()
    {
        String username = "gica";
        BrokerBL brokerBL = new BrokerBL();
        assertEquals(0, brokerBL.getBrokerId(username));
    }
    
    @Test
    public void getMoney()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        BrokerBL brokerBL = new BrokerBL();
        int id = brokerBL.getBrokerId(username);
        broker.setIdBroker(id);
        brokerBL.loadMoney(broker);
        assertEquals(999700.0, broker.getMoney());
    }
    
    @Test
    public void loadInstruments()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        BrokerBLHB brokerBLHB = new BrokerBLHB();
        brokerBLHB.loadInstruments(broker);
        assertEquals(1, broker.getInstruments().size());
    }
    
    @Test 
    public void loadTrades()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        BrokerBLHB brokerBLHB = new BrokerBLHB();
        brokerBLHB.loadTrades(broker);
        assertEquals(2, broker.getTrades().size());
    }
    
    @Test public void loadSellTrades()
    {
        //testul depinde si de ziua in care se testeaza.
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        BrokerBLHB brokerBLHB = new BrokerBLHB();
        int id = brokerBLHB.getBrokerId(username);
        broker.setIdBroker(id);
        List<Trade> sellTrades = brokerBLHB.getSellTrades(broker);
        assertEquals(1, sellTrades.size());
    }
    
    @Test public void deleteInstrument()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        int idTrade = 1;
        BrokerBLHB brokerBLHB = new BrokerBLHB();
        int id = brokerBLHB.getBrokerId(username);
        broker.setIdBroker(id);
        brokerBLHB.deleteInstrument(idTrade);
        brokerBLHB.loadInstruments(broker);
        assertEquals(0, broker.getInstruments().size());
    }
    
    @Test public void deleteInstrument2()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        int idTrade = 1;
        BrokerBL brokerBL = new BrokerBL();
        int id = brokerBL.getBrokerId(username);
        broker.setIdBroker(id);
        brokerBL.deleteInstrument(idTrade);
        brokerBL.loadInstruments(broker);
        assertEquals(0, broker.getInstruments().size());
    }
    
    @Test public void updatePrice()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        BrokerBL brokerBL = new BrokerBL();
        brokerBL.loadMoney(broker);
        double money = 1000.0;
        brokerBL.updateBrokerPrice(broker, money);
        assertEquals(1000.0, broker.getMoney());
    }
    
    @Test public void updatePrice2()
    {
        String username = "gica";
        String pass = "gica";
        Broker broker = new Broker(username, pass);
        BrokerBLHB brokerBLHB = new BrokerBLHB();
        brokerBLHB.loadMoney(broker);
        double money = 1000.0;
        brokerBLHB.updateBrokerPrice(broker, money);
        assertEquals(1000.0, broker.getMoney());
    }
}
